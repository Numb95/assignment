FROM python:3.7
WORKDIR /app
COPY requirements.txt .
RUN pip install -r requirements.txt && rm -Rf requirements.txt
COPY app.py .
