---
weight: 5
---
# Monitoring and logging 

## Monitoring
Several sections need to be monitored. I will describe each item. 
- Monitor the HTTP Endpoint of the application. As an example, https://domain.com/encrypt or https://domain.com/decrypt should be monitored with the help of an external monitoring tool to ensure they get 200 HTTP requests. 

- The readiness probe and the liveness probe should be applied. If there's an issue on deployment, it will send and alert to alerting system

- A Healthcheck (via an HTTP endpoint, such as /health) should be implemented to ensure all application modules are working properly. 

- A metric for application should be developed. It can monitor number of hits, failed encryption or decryption, HTTP request errors, response time, and latency, and send them to a Metric server (like Prometheus).

## Logging
The following line logs everything for this application:

```python
logging.basicConfig(filename='app.log', level=logging.INFO)
```
This is totally incorrect. Because it contains applications logging in a file called app.log. The file will be deleted when the application container is restarted. logging.INFO is also not a good log level. The system will log everything. Let me change this to something like this:

```python
import sys
logging.basicConfig(stream=sys.stdout, level=logging.ERROR, format='%(asctime)s %(levelname)s:%(message)s')
```
Streaming logs to stdout helps external services (Datadog, fluentd) collect logs. Change log level to Error. Only errors in production are needed. We do not care about encryption or decryption requests. We will be able to figure out what happened on a specific time by formatting the logs with log level and time. This changes applied to code. The following is a sample log after applying this method:
```
2021-08-03 01:01:05,677 INFO:172.17.0.1 - - [03/Aug/2021 01:01:05] "POST /encrypt HTTP/1.1" 200 -
```
Because of be in the track of application (which print INFO:Encrypting while encryption), I just change level=logging.ERROR to level=logging.INFO. For production it's better to have logging.ERROR
Furthermore, I think it would be a great idea to output logs to JSON, which is outside the scope of this project.