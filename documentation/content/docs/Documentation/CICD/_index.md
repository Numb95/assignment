---
weight: 3
---

# CI/CD
Before start, I want to mention that most of this pipeline will be run manually because if there's an issue before, It won't run on production.
## .gitlab-ci.yml Structure
```yaml
stages:
    - build
    - security
    - deploy
    - doc

variables:
    IMG_ADDR_MAIN_CURRENT: registry.gitlab.com/numb95/assignment:$CI_COMMIT_TAG
    IMG_ADDR_MAIN_LATEST: registry.gitlab.com/numb95/assignment:latest
    GIT_SUBMODULE_STRATEGY: recursive


build-docker:
    image: docker:latest
    stage: build
    services:
        - docker:dind
    before_script:
        - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    script:
        - docker build -t $IMG_ADDR_MAIN_CURRENT .
        - docker tag $IMG_ADDR_MAIN_CURRENT $IMG_ADDR_MAIN_LATEST
        - docker push $IMG_ADDR_MAIN_CURRENT
        - docker push $IMG_ADDR_MAIN_LATEST
    only:
        - tags

deploy-helm:
    stage: deploy
    image: numb95/gitlab-ci-deploy-tools
    script:
        - mkdir -p /etc/deploy
        - echo ${kube_config} | base64 -d > ${KUBECONFIG}
        - kubectl config use-context production
        - if ! kubectl get namespaces -o json | jq -r ".items[].metadata.name" | grep assignment-ns ;then kubectl create ns assignment-ns; fi
        - helm upgrade --atomic --install --timeout 60s -f deployment/values.yaml assignment-dep ./deployment -n assignment-ns
    only:
        - tags
    when: manual 

pages:
    stage: doc
    image: registry.gitlab.com/pages/hugo:latest
    before_script:
        - apk add --update --no-cache git
    script:
        - ls 
        - cd documentation
        - hugo 
    artifacts:
      paths:
        - public
    only:
        - tags

docker-vul-scan:
    stage: security
    image: alpine:latest
    before_script:
        - apk add --update --no-cache curl git bash
        - mkdir $(pwd)/trivyinfo
        - curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/install.sh | sh -s -- -b /usr/local/bin v0.19.2
    script:
        - export NOW=$(date +%Y_%m_%d_%H_%M_%S)
        - trivy image $IMG_ADDR_MAIN_CURRENT > $(pwd)/trivyinfo/$NOW.txt
    artifacts:
      untracked: false
      expire_in: 30 days
      paths:
        - trivyinfo
    only: 
        - tags
    when: manual

dockerfile-vul-scan:
    stage: security
    image: alpine:latest
    before_script:
        - apk add --update --no-cache curl git bash
        - curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/install.sh | sh -s -- -b /usr/local/bin v0.19.2
    script:
        - trivy fs --security-checks vuln,config $(pwd)
    only: 
        - tags
    when: manual 

code-security:
    stage: security
    image: python:3.9
    allow_failure: true
    before_script:
         - pip install bandit
    script:
        - bandit $(pwd)/app.py
    only:
        - tags
    when: manual

kube-sec:
    stage: security
    image: alpine:latest
    before_script:
        - apk add --update --no-cache wget curl git bash openssl
        - wget https://github.com/controlplaneio/kubesec/releases/download/v2.11.2/kubesec_linux_amd64.tar.gz && tar -xvzf kubesec_linux_amd64.tar.gz  && chmod a+x kubesec && mv kubesec /usr/local/bin/
        - curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash
    script:
        - helm template ./deployment > /tmp/deployment.yaml
        - kubesec scan /tmp/deployment.yaml
    only:
        - tags
    when: manual

```

# Description

Each section will be described, along with why I chose this method

## Early stages
```yaml
stages:
    - build
    - deploy
    - doc
variables:
    IMG_ADDR_MAIN_CURRENT: registry.gitlab.com/numb95/assignment:$CI_COMMIT_TAG
    IMG_ADDR_MAIN_LATEST: registry.gitlab.com/numb95/assignment:latest
```
 The first thing we do is declare our stages. There are three main stages. There is a build stage for building, a deploy stage for deploying, and a doc stage for generating documents. Due to the ease of managing docker images and the build mechanism, I use two environment variables as docker image names. $CI_COMMIT_TAG can be used to get the tag name of docker image from git tag (which is better for managing versions) and latest which  used for pushing to latest tag of docker image. So, we used the git tag and the gitlab environment ($CI_COMMIT_TAG) to set our tag. Gitlab is my choice for a docker registry because it's a reliable and easy-to-use registry. Furthermore, the code is hosted on Gitlab, so it's easy to manage the Docker image.

## Build Section
```yaml
build-docker:
    image: docker:latest
    stage: build
    services:
        - docker:dind
    before_script:
        - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    script:
        - docker build -t $IMG_ADDR_MAIN_CURRENT .
        - docker tag $IMG_ADDR_MAIN_CURRENT $IMG_ADDR_MAIN_LATEST
        - docker push $IMG_ADDR_MAIN_CURRENT
        - docker push $IMG_ADDR_MAIN_LATEST
    only:
        - tags
```
In this section, we build out docker image. First we need to use docker:latest as default image for running this pipeline. As we know before, this is part of the build process so we assign it to build stage. For building image we need a docker service so we use docker:dind service. Also, we need to login to our docker registry (which is Gitlab container registry), So it's best fit to do it it before_script section. registry user ,password and registry URL will be passed to pipeline from Gitlab-CI predefined environment variable ($CI_REGISTRY_USER, $CI_REGISTRY_PASSWORD and $CI_REGISTRY). Other parts are obvious. Just building docker image based on our environment variables which we set before, tag curremt version to latest and push them to docker registry.

## Deploy Section

```yaml 
deploy-helm:
    stage: deploy
    image: numb95/gitlab-ci-deploy-tools
    script:
        - echo ${kube_config} | base64 -d > ${KUBECONFIG}
        - kubectl config use-context production
        - if ! kubectl get namespaces -o json | jq -r ".items[].metadata.name" | grep assignment-ns ;then kubectl create ns assignment-ns; fi
        - helm upgrade --atomic --install --timeout 60s -f deployment/values.yaml assignment-dep ./deployment -n assignment-ns
        
    only:
        - tags
    when: manual
```
In deploy section, I set stage to deploy. Also, I have a greate docker image (which is available [here](https://hub.docker.com/r/numb95/gitlab-ci-deploy-tools)) which had lots of useful tools for deploying any kind of application. So, I used it as default docker image for deploy. in script section, I set $KUBECONFIG environment variable to a value which get from $kube_config (which is a user defined base64 kubeconfig), then decode it from base64 and set it to $KUBECONFIG. In next section I write a simple bash script which get namespace, if it doesnot exist, it will create it, if exist it's just pass. Next line will deploy application via helm. It's a atomic deploy, So if the deploy process failed, the helm uninstall it. If it's not installed --install will install it. Also, 60 seconds of timeout applied. other parameters like -f (which get value files from a values.yaml file), name of the application in helm (which is assignment-dep), set the directory of deployment files(./deployment) and setting the namespace (-n assignment-ns) applied.

## Document Section

```yaml
generate-doc:
    stage: doc
    image: registry.gitlab.com/pages/hugo:latest
    script:
        - cd documentation
        - hugo 
    artifacts:
      paths:
        - public
    only:
        - main
    when: manual
```
I used hugo as documentation(which is a great static generator which a greate template language) as documentation backend. I set the stage to doc, use gitlab hugo docker image as pipeline default image, build the documentations and store it as an artifacts. So, with gitlab pages it is available via the URL.

## Security section
All the pipelines in security section, are part of securiity document which I implement them into CI/CD