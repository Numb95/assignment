---
weight: 4
---

# Security
In terms of security, I believe I need to check three layers of the application. First, we need to check every stage of the application for vulnerabilities. It starts with the code and ends with the deployment files. Then any security issues on the code layer, then any security issues on the network layer (HTTP)




## Vulnerabilities

I will check for vulnerabilities in Dockerimages, Code, and Kubernetes objects. I will describe all three items here.

### Docker image vulnerability scanning [add to CI]
I use trivy to check docker image vulnerabilities. It is a great tool for this job. The job is available on the CI/CD section. After installing the trivy ([check this link](https://aquasecurity.github.io/trivy/v0.19.2/getting-started)) I run this command:
```bash
trivy i registry.gitlab.com/numb95/assignment:latest
```
Which result [this](https://gitlab.com/-/snippets/2156473). On OS level (Docker image), the report shows 13 critical security issues. Several of them will affect our application, so we need to update them. Here is a sample where libc has a critical issue that needs to be fixed.
```
+------------------------------+---------------------+----------+------------------------------+----------------+--------------------------------------------------------------+
| libc-bin                     | CVE-2021-33574      | CRITICAL | 2.28-10                      |                | glibc: mq_notify does                                        |
|                              |                     |          |                              |                | not handle separately                                        |
|                              |                     |          |                              |                | allocated thread attributes                                  |
|                              |                     |          |                              |                | -->avd.aquasec.com/nvd/cve-2021-33574                        |
+                              +---------------------+----------+                              +----------------+--------------------------------------------------------------+
```
Let's check the Dockerfile for vulnerabilities. Therefore, we need to run a filesystem scan:
```bash
trivy fs --security-checks vuln,config .
```
which result this:
```
Dockerfile (dockerfile)
=======================
Tests: 23 (SUCCESSES: 22, FAILURES: 1, EXCEPTIONS: 0)
Failures: 1 (UNKNOWN: 0, LOW: 0, MEDIUM: 0, HIGH: 1, CRITICAL: 0)

+---------------------------+------------+-----------+----------+------------------------------------------+
|           TYPE            | MISCONF ID |   CHECK   | SEVERITY |                 MESSAGE                  |
+---------------------------+------------+-----------+----------+------------------------------------------+
| Dockerfile Security Check |   DS002    | root user |   HIGH   | Specify at least 1 USER                  |
|                           |            |           |          | command in Dockerfile with               |
|                           |            |           |          | non-root user as argument                |
|                           |            |           |          | -->avd.aquasec.com/appshield/ds002       |
+---------------------------+------------+-----------+----------+------------------------------------------+
```
In the future, we need to change the Docker image User. For now, we just need to figure out what the problem is. 

### Code vulnerabilities scanning
We can use SAST and DAST in Gitlab-ci for this section. I skip them to save time. Take a look at these two links for more information: [SAST](https://docs.gitlab.com/ee/user/application_security/sast/) and [DAST](https://docs.gitlab.com/ee/user/application_security/dast/)


#### bandit
[Bandit](https://pypi.org/project/bandit/) is a great tool for checking code for vulnerabilities. Here is the result:
```[main]  INFO    profile include tests: None
[main]  INFO    profile exclude tests: None
[main]  INFO    cli include tests: None
[main]  INFO    cli exclude tests: None
[main]  INFO    running on Python 3.9.2
Run started:2021-08-02 22:33:03.701302

Test results:
>> Issue: [B413:blacklist] The pyCrypto library and its module AES are no longer actively maintained and have been deprecated. Consider using pyca/cryptography library.
   Severity: High   Confidence: High
   Location: /Users/goodarz/Downloads/tiqets-devops-test/app.py:2
   More Info: https://bandit.readthedocs.io/en/latest/blacklists/blacklist_imports.html#b413-import-pycrypto
1       from flask import Flask, request
2       from Crypto.Cipher import AES
3       from Crypto import Random

--------------------------------------------------
>> Issue: [B413:blacklist] The pyCrypto library and its module Random are no longer actively maintained and have been deprecated. Consider using pyca/cryptography library.
   Severity: High   Confidence: High
   Location: /Users/goodarz/Downloads/tiqets-devops-test/app.py:3
   More Info: https://bandit.readthedocs.io/en/latest/blacklists/blacklist_imports.html#b413-import-pycrypto
2       from Crypto.Cipher import AES
3       from Crypto import Random
4       import base64

--------------------------------------------------

Code scanned:
        Total lines of code: 31
        Total lines skipped (#nosec): 0

Run metrics:
        Total issues (by severity):
                Undefined: 0.0
                Low: 0.0
                Medium: 0.0
                High: 2.0
        Total issues (by confidence):
                Undefined: 0.0
                Low: 0.0
                Medium: 0.0
                High: 2.0
Files skipped (0):
```
Obviously, it says pycrypto is deprecated (as I said earlier in the Dockerfile section), so we need to change this library. Other tools like (safety)[https://pyup.io/safety/] mention this issue.
### Kubernetes objects vulnerabilities scanning
[kubesec-scan](https://github.com/controlplaneio/kubectl-kubesec) is the tool that I use to check for vulnerabilities in Kubernetes objects. Here is the result:

```
❯ kubectl kubesec-scan -n assignment-ns deployment assignment-dep-assignment-deployment
scanning deployment assignment-dep-assignment-deployment in namespace ass
kubesec.io score: 7
-----------------
Advise1. containers[] .securityContext .runAsNonRoot == true
Force the running image to run as a non-root user to ensure least privilege
2. containers[] .securityContext .capabilities .drop
Reducing kernel capabilities available to a container limits its attack surface
3. containers[] .securityContext .readOnlyRootFilesystem == true
An immutable root filesystem can prevent malicious binaries being added to PATH and increase attack cost
4. containers[] .securityContext .runAsUser > 10000
Run as a high-UID user to avoid conflicts with the host's user table
5. containers[] .securityContext .capabilities .drop | index("ALL")
Drop all capabilities and add only those required to reduce syscall attack surface
```
Therefore, it's better to fix all of these issues. Like number 4 says that container runs as a user greater than 10000, which needs to be changed in order to avoid any conflicts with host's user table.

## Deep dive in code
There are several things in the code that need to be improved. There's a line that gets APP_AES_KEY environment key as a variable. Encrypting or decrypting a key in this way is not secure. 
```python
encrypt_key = os.getenv('APP_AES_KEY')
```
This key should be retrieved from an external secret management system. Using this snippet, we can retrieve the secret from the vault to secure this part:

```python
import os
import hvac

client = hvac.Client()
client = hvac.Client(
 url=os.environ['VAULT_URL'],
 token=os.environ['VAULT_TEMP_TOKEN']
encrypt_key = client.read('secret/encryption-key')
```
VAULT_TEMP_TOKEN is a temporary key that will expire after a specific amount of time. In order to decrypt secret, a new value of VAULT_TEMP_TOKEN must be entered whenever it is needed. It can be an environment variable, a key in a file, or an input from the user. The method is not implemented in the application because it requires some extra tools.
## Network scanning
It should be run by a security expert (which I am not). Yet, it should examine all possibilities (like XSS) to figure out where the problem is and fix it.