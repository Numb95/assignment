---
weight: 2
---

# Deployment

## Introduction
In order to deploy, I use Helm, because of its ease of deployment, ease of rollback and ease of adding Kubernetes objects using its template language. The deployment directory contains several files, as you can see. Values.yaml is the most important file. Values are stored here. Take a look at this file.

## values.yaml Structure
```yaml
replicaCount: 1
image:
  repository: registry.gitlab.com/numb95/assignment
  pullPolicy: IfNotPresent
  tag: ""
imagePullSecrets: []
nameOverride: ""
fullnameOverride: ""
serviceAccount:
  create: true
  annotations: {}
  name: ""
podAnnotations: {}
podSecurityContext: {}
securityContext: {}
service:
  type: ClusterIP
  port: 5000
ingress:
  enabled: false
  annotations: {}
    # kubernetes.io/ingress.class: nginx
    # kubernetes.io/tls-acme: "true"
  hosts:
    - host: assignment.local
      paths: [] 
  tls: []
resources: 
  limits:
    cpu: 100m
    memory: 128Mi
  requests:
    cpu: 100m
    memory: 128Mi
autoscaling:
  enabled: false
  minReplicas: 1
  maxReplicas: 100
  targetCPUUtilizationPercentage: 80
nodeSelector: {}
tolerations: []
affinity: {}
containercommand:
  command: '["flask"]'
  args: '["run"]'
envvalues:
  configmap: assignment-configmap
  secret: assignment-secret
env:
  Configmap:
    TZ: /etc/localtime
    FLASK_ENV: production
    FLASK_APP: app.py
  Secret:
    APP_KEY: c29tZWtleTIzNDg3NjI4Mwo=
expose:
  port: 5000
  protocol: TCP
```

# Description

In this file I set the desirable values like image address, type of service, resource limits, container command, name of secret and configmap which uses as environment variable off container.riable off container. a Sample APP_AES_KEY variable applied to deployment/template/secret.yaml which will be changed after deploy and container ports. 
Kubernetes objects such as HPA or ingress have been added to the deployment/template directory, which can be used in the future. Adding then to vaules.yaml is all we need to do, Also, I mount /etc/localtime and /etc/timezone of the host to the container for better time management. Look at this snippet:
```yaml
      volumes:
        - name: localtime-vol
          hostPath:
            path: /usr/share/zoneinfo/Europe/Amsterdam
        - name: timezone-vol
          hostPath:
            path: /etc/timezone
            type: File
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
          command: {{ .Values.containercommand.command }}
          args: {{ .Values.containercommand.args }}
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          envFrom:
          - configMapRef:
              name: {{ .Values.envvalues.configmap }}
          - secretRef:
              name: {{ .Values.envvalues.secret }}
          ports:
            - name: http
              containerPort: {{ .Values.expose.port }}
              protocol: {{ .Values.expose.protocol }}
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
          volumeMounts:
          - name: localtime-vol
            mountPath: /etc/localtime
          - name: timezone-vol
            mountPath: /etc/timezone
```
## Technical decisions
Let's talk about some of my deployment decisions. As I said before, I use helm because ease of deployment, ease of version management and rollback, ease of adding Kubernetes objects and decrease complexity for teams. Some folks said: Helm Charts provide “push button” deployment and deletion of apps.

I have always had the idea of resource limitations. By using this, you can reduce the costs of infrastructure and decrease the risk of outages in other applications. Therefore, I applied a resource limit to the project. 

I also use Kubernetes default secret management for ease of application. For production, I suggest AWS Secret Manager or vaults as better secret management tools. Due to my belief in the 12factor, I use all of the application's variables as environment variables


