---
weight: 1
---

# Dockerfile

## Dockerfile Structure
```Dockerfile
FROM python:3.7
WORKDIR /app
COPY requirements.txt .
RUN pip install -r requirements.txt && rm -Rf requirements.txt
COPY app.py .
ENTRYPOINT [ "flask" ]
CMD [ "run", "--host", "0.0.0.0"]
```

## Line By Line description
* `FROM python:3.7`
> The base image should be python:3.7. I used Python 3.7 instead of Python 3.9 or Python 3.8 due to some conflicts that I will describe later. There's a library called pycrypto which is currently discontinued and another fork called pycryptodome is actively developing. Pycryptodome requires major changes to code, so I chose to use pycrypto with a lower version of Python. Why do I do that? In Python 3.8+, the time.clock() function is deprecated. Python 3.8+ uses the time.time() function.  Pycrypto uses time.clock(), so we have to change the source code, which is painful, so I decided to use Python 3.7 to resolve this issue.
* `WORKDIR /app`
> For better management, change working directory to /app
* ‍‍‍‍‍`COPY requirements.txt .‍`
> Copy `requirements.txt` to install dependencies 
* `RUN pip install -r requirements.txt && rm -Rf requirements.txt`
> Install dependencies and delete *requirements.txt* for better security and to avoid leaking any application information.
* `COPY app.py .`
> Copy Application file to `/app` directory
* `ENTRYPOINT [ "flask" ]`
> Set entrypoint to flask   
* `CMD [ "run", "--host", "0.0.0.0"]`
> CMD for run application. `FLASK_APP`, `APP_AES_KEY` are passed to the application via the environment variable as configmap or secret. For future debugging, set the FLASH_ENV variable. *TZ* is set to */etc/localtime*, which is mounted to */etc/localtime* of workers, so there is no need to deal with time in containers.

For [some reason](https://pythonspeed.com/articles/alpine-docker-python/), I use a Docker image based on Debian, which is about 300MB+ in size. We can improve it and reduce its size in the future